import * as Yargs from 'yargs';

import { parseNetworkFormat, parseNeuralNetwork } from './util/util';

import { splitDataset } from './commands/split-dataset';
import { newNeuralNetwork } from './commands/new-nn';
import { trainNeuralNetwork } from './commands/train-neural-network';
import { testNeuralNetwork } from './commands/test-neural-network';

Yargs
    .command(
        'train [train-data-path]',
        'train the network using data from the specified path',
        (yargs) => {
            yargs
                .positional('train-data-path', {
                    describe: 'path of train data. Must be in the csv format',
                    type: 'string',
                    demandOption: true,
                })
                .option('num-epochs', {
                    describe: 'number of epochs to be used',
                    default: 1,
                    type: 'number',
                })
                .option('learning-rate', {
                    describe: 'learning rate to be used',
                    default: 0.2,
                    type: 'number',
                })
                .option('validation-data-path', {
                    describe: 'path to validation data to be used in the error calculation after each epoch',
                    type: 'string',
                });
        },
        trainNeuralNetwork,
    )
    .command(
        'test [test-data-path]',
        'test the network using data from the specified path',
        (yargs) => {
            yargs
                .positional('test-data-path', {
                    describe: 'path of test data. Must be in the csv format',
                    type: 'string',
                    demandOption: true,
                })
                .option('export-result-path', {
                    describe: 'path to export results to later use',
                    type: 'string',
                });
        },
        testNeuralNetwork,
    )
    .command(
        'split-dataset [dataset-path]',
        'split the specified dataset into training, testing and verification',
        (yargs) => {
            yargs
                .positional('dataset-path', {
                    describe: 'path of dataset. Must be in the csv format',
                    type: 'string',
                    demandOption: true,
                })
                .option('num-outputs', {
                    describe: 'number of outputs on dataset',
                    type: 'number',
                    demandOption: true,
                })
                .option('num-train', {
                    describe: 'number of training values to split',
                    type: 'number',
                })
                .option('num-test', {
                    describe: 'number of test values to split',
                    type: 'number',
                })
                .option('num-validation', {
                    describe: 'number of validation values to split',
                    type: 'number',
                });
        },
        splitDataset,
    )
    .command(
        'new',
        'create a new random network and exports it',
        yargs => { yargs; },
        newNeuralNetwork,
    )
    .option('neural-network', {
        alias: 'i',
        describe: 'path to previously exported network to use. If it is not specified, it will be generated randomly',
        type: 'string',
        coerce: parseNeuralNetwork,
    })
    .option('network-format', {
        alias: 'f',
        describe: 'the format of the network. Must be "num-inputs [num-hidden-1 [num-hidden-2 [num-hidden-3 ...] ] ] num-outputs"',
        type: 'string',
        coerce: parseNetworkFormat,
    })
    .option('export-network', {
        alias: 'o',
        describe: 'path to export network to later use',
        type: 'string',
    })
    .config()
    .argv;
