import { Tensor, randomUniform, sigmoid, Rank, sub, TensorLike, tensor } from '@tensorflow/tfjs-node';
import { ISerializedLayer } from '../interface/serialized.interface';

export class Layer {
    weights: Tensor;
    bias: Tensor;

    constructor(public numInputs: number, public numOutputs: number) {
        this.weights = randomUniform([numOutputs, numInputs], -1, 1);
        this.bias = randomUniform([numOutputs, 1], -1, 1);
    }

    static Activation(input: Tensor<Rank>): Tensor<Rank> {
        return sigmoid(input);
    }

    static ActivationDerivative(input: Tensor<Rank>): Tensor<Rank> {
        return input.mul(sub(1, input));
    }

    predict(input: TensorLike | Tensor): Tensor<Rank> {
        const rInput = input instanceof Tensor
            ? input
            : tensor(input).reshape([this.numInputs, 1]);

        return Layer.Activation(this.weights.matMul(rInput).add(this.bias));
    }

    train(input: TensorLike, output: TensorLike, learningRate: number): void {
        const desiredOutput = tensor(output).reshape([this.numOutputs, 1]);
        const rInput = input instanceof Tensor
            ? input
            : tensor(input).reshape([this.numInputs, 1]);

        const predictedOutput = this.predict(input);

        const errorDerivative = predictedOutput.mul(2).sub(desiredOutput.mul(2));
        const predictedDerivative = Layer.ActivationDerivative(predictedOutput);

        const weightGradient = errorDerivative.mul(predictedDerivative).matMul(rInput.transpose());
        const biasGradient = errorDerivative.mul(predictedDerivative);

        this.weights = this.weights.sub(weightGradient.mul(learningRate));
        this.bias = this.bias.sub(biasGradient.mul(learningRate));
    }

    // Serialization stuff

    static fromObject(value: ISerializedLayer): Layer {
        const layer = new Layer(value.numInputs, value.numOutputs);
        layer.weights = tensor(value.weights);
        layer.bias = tensor(value.bias);
        return layer;
    }

    toObject(): ISerializedLayer {
        return {
            numInputs: this.numInputs,
            numOutputs: this.numOutputs,
            weights: this.weights.arraySync() as number[][],
            bias: this.bias.arraySync() as number[][],
        };
    }

}