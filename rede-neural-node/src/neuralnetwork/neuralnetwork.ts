import { Tensor, Rank } from '@tensorflow/tfjs-node';

import { ISerializedNeuralNetwork } from '../interface/serialized.interface';
import { Layer } from './layer';

export class NeuralNetwork {
    private layers: Layer[];

    constructor(public numInputs: number, public hiddenLayers: number[], public numOutputs: number) {
        const allLayers = hiddenLayers.concat(numOutputs);

        this.layers = [];
        this.layers.push(new Layer(numInputs, allLayers[0]));
        for (let i = 1; i < allLayers.length; i++) {
            this.layers.push(new Layer(allLayers[i - 1], allLayers[i]));
        }
    }

    predict(input: Tensor<Rank>): Tensor<Rank> {
        let output = input.transpose();
        for (const layer of this.layers) {
            output = layer.predict(output);
        }
        return output.transpose();
    }

    train(input: Tensor<Rank>, output: Tensor<Rank>, length: number, learningRate: number): void {
        const desiredOutput = output
            .reshape([this.numOutputs, length]);

        // ===== Feed Forward =====
        const hs: Tensor<Rank>[] = [];
        hs.push(input.reshape([this.numInputs, length]));
        for (const layer of this.layers) {
            const lastPredicted = hs[hs.length - 1];
            hs.push(layer.predict(lastPredicted));
        }

        // ===== Back Propagation =====
        const findNewValues = (
            errorDerivative: Tensor<Rank>,
            layer: Layer,
            currentH: Tensor<Rank>,
            previousH: Tensor<Rank>,
        ): Tensor<Rank> => {
            const delta = errorDerivative.mul(Layer.ActivationDerivative(currentH));

            layer.weights = layer.weights.sub(delta.matMul(previousH.transpose()).mul(learningRate));
            layer.bias = layer.bias.sub(delta.mul(learningRate).sum(1, true));

            return delta;
        };

        let delta: Tensor<Rank>;
        let errorDerivative: Tensor<Rank>;

        // Last layer different error derivative
        const lastLayer = this.layers[this.layers.length - 1];
        const lastH = hs[hs.length - 1];
        const lastLastH = hs[hs.length - 2];

        errorDerivative = lastH.mul(2).sub(desiredOutput.mul(2));
        delta = findNewValues(errorDerivative, lastLayer, lastH, lastLastH);

        // Rest of the layers
        for (let i = this.layers.length - 2; i >= 0; i--) {
            const hsi = i + 1;

            errorDerivative = this.layers[i + 1].weights.transpose().matMul(delta);

            delta = findNewValues(errorDerivative, this.layers[i], hs[hsi], hs[hsi - 1]);
        }
    }

    // Serialization stuff

    static fromObject(value: ISerializedNeuralNetwork): NeuralNetwork {
        const nn = new NeuralNetwork(value.numInputs, value.hiddenLayers, value.numOutputs);
        nn.layers = value.layers.map(sl => Layer.fromObject(sl));
        return nn;
    }

    toObject(): ISerializedNeuralNetwork {
        return {
            numInputs: this.numInputs,
            numOutputs: this.numOutputs,
            hiddenLayers: this.hiddenLayers,
            layers: this.layers.map(l => l.toObject()),
        };
    }

}
