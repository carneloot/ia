import { Rank, Tensor, where, equal, max, ones, zeros, tensor } from '@tensorflow/tfjs-node';

import * as fs from 'fs';

import { INetworkFormat } from '../interface/network-format.interface';
import { ISerializedNeuralNetwork } from '../interface/serialized.interface';

import { NeuralNetwork } from '../neuralnetwork';
import { IDataSet } from '../interface/dataset.interface';

export const parseNetworkFormat = (value: string): INetworkFormat => {
    const splittedString = value.split(' ');

    const inputLayer: number = parseInt(splittedString.splice(0, 1)[0], 10);
    const outputLayer: number = parseInt(splittedString.splice(-1, 1)[0], 10);
    const hiddenLayers: number[] = splittedString.map(value => parseInt(value, 10));

    return {
        inputLayer,
        outputLayer,
        hiddenLayers,
    };
};

export const parseNeuralNetwork = (path: string): NeuralNetwork => {
    const nnFile = fs.readFileSync(path);

    const nnObj: ISerializedNeuralNetwork = JSON.parse(nnFile.toString());

    return NeuralNetwork.fromObject(nnObj);
};

export const readFile = (path: string): string[] => fs
    .readFileSync(path, { flag: 'r' })
    .toString()
    .split('\n')
    .filter(s => s !== '');

export const saveFile = (path: string, content: string): void => {
    fs.writeFileSync(path, content);
};

export const openDataSet = (path: string, numInput: number, numOutput: number): IDataSet => {
    const { inValues, outValues, length } = readFile(path)
        .map(line => line
            .split(',')
            .map(v => parseFloat(v))
        )
        .reduce((acc, value) => {
            acc.inValues.push(value.slice(0, numInput));
            acc.outValues.push(value.slice(numInput, numInput + numOutput));
            acc.length += 1;
            return acc;
        }, {
            inValues: [],
            outValues: [],
            length: 0,
        });

    const inputs = tensor(inValues);
    const outputs = tensor(outValues);

    return { inputs, outputs, length };
};

export const saveNetworkToFile = (path: string, network: NeuralNetwork | ISerializedNeuralNetwork): void => {
    console.log(`Exporting network to "${path}"...`);
    const obj = (network instanceof NeuralNetwork)
        ? network.toObject()
        : network;

    fs.writeFileSync(path, JSON.stringify(obj));
};

export const initNetwork = (neuralNetwork?: NeuralNetwork, networkFormat?: INetworkFormat): NeuralNetwork => {
    if (!networkFormat && !neuralNetwork) {
        throw Error('Please provide a starting network state');
    }

    if (neuralNetwork) {
        return neuralNetwork;
    }

    const { inputLayer, hiddenLayers, outputLayer } = networkFormat;
    return new NeuralNetwork(inputLayer, hiddenLayers, outputLayer);
};
