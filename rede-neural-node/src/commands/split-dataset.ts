import { readFile, saveFile } from '../util/util';

export interface SplitDatasetArgs {
    datasetPath: string;
    numTrain: number;
    numTest: number;
    numOutputs: number;

    numValidation?: number;
}

const randInt = (min: number, max: number): number => min + Math.floor(Math.random() * (max - min));

const shuffle = <T>(array: T[]): T[] => {
    const shuffled: T[] = [];

    for (let i = 0; i < array.length; i++) {
        shuffled.push(array[randInt(0, array.length)]);
    }

    return shuffled;
};

const splitFile = (file: string[][], size: number): string[] => {
    const result: string[] = [];

    while (result.length < size) {
        for (let j = 0; j < file.length; j++) {
            result.push(file[j].splice(randInt(0, file[j].length), 1)[0]);
        }
    }

    return shuffle(result);
};

export const splitDataset = (args: SplitDatasetArgs): void => {
    const fileLines = readFile(args.datasetPath);
    const basePath = args.datasetPath.replace('.csv', '');

    const separatedOutputs: string[][] = [];
    for (let i = 0; i < args.numOutputs; i++) {
        const outputStr = Array.from(Array(args.numOutputs).keys())
            .map((value) => value === i ? '1' : '0')
            .join(',');

        separatedOutputs.push(fileLines.filter(line => line.endsWith(outputStr)));
    }

    const trainData = splitFile(separatedOutputs, args.numTrain);
    const trainDataFilename = `${basePath}-train.csv`;
    saveFile(trainDataFilename, trainData.concat('').join('\n'));

    const testData = splitFile(separatedOutputs, args.numTest);
    const testDataFilename = `${basePath}-test.csv`;
    saveFile(testDataFilename, testData.concat('').join('\n'));

    if (args.numValidation) {
        const validationData = splitFile(separatedOutputs, args.numValidation);
        const validationDataFilename = `${basePath}-validation.csv`;
        saveFile(validationDataFilename, validationData.concat('').join('\n'));
    }
};
