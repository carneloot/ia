import * as yesno from 'yesno';

import { NeuralNetwork } from '../neuralnetwork';
import { INetworkFormat } from '../interface/network-format.interface';
import { openDataSet, saveNetworkToFile, initNetwork } from '../util/util';
import { IDataSet } from '../interface/dataset.interface';
import { ISerializedNeuralNetwork } from '../interface/serialized.interface';

export interface TrainNeuralNetworkArgs {
    trainDataPath: string;

    numEpochs: number;
    learningRate: number;

    validationDataPath?: string;

    neuralNetwork?: NeuralNetwork;
    networkFormat?: INetworkFormat;

    exportNetwork?: string;
}

const calculateValidationError = (network: NeuralNetwork, data: IDataSet): number => {
    const { inputs, outputs: desiredOutputs } = data;
    const predictedOutputs = network.predict(inputs);

    const error = predictedOutputs.sub(desiredOutputs);
    const sumError = error.abs().sum();

    return sumError.arraySync() as number;
};

export const trainNeuralNetwork = async (args: TrainNeuralNetworkArgs): Promise<void> => {
    if (!args.exportNetwork) {
        const shouldContinue = await yesno({
            question: 'The neural network will not be saved because no export path was provided. Are you sure you want to continue? [Y/n]',
            defaultValue: true,
        });

        if (!shouldContinue) {
            return;
        }
    }

    const network = initNetwork(args.neuralNetwork, args.networkFormat);

    const trainData = openDataSet(args.trainDataPath, network.numInputs, network.numOutputs);
    const validationData = args.validationDataPath && openDataSet(args.validationDataPath, network.numInputs, network.numOutputs) || null;

    let bestNetwork: ISerializedNeuralNetwork = null;
    let bestError = Infinity;

    console.log(`Training network for ${args.numEpochs} epochs...`);

    const learningRate = args.learningRate / trainData.length;
    for (let i = 0; i < args.numEpochs; i++) {
        network.train(trainData.inputs, trainData.outputs, trainData.length, learningRate);

        if (validationData) {
            const error = calculateValidationError(network, validationData);
            if (error < bestError) {
                bestNetwork = network.toObject();
                bestError = error;
            }
        }
    }

    if (args.exportNetwork) {
        saveNetworkToFile(args.exportNetwork, bestNetwork || network);
    }
};
