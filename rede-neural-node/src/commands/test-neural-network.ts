import { math } from '@tensorflow/tfjs-node';

import { NeuralNetwork } from '../neuralnetwork';
import { INetworkFormat } from '../interface/network-format.interface';
import { openDataSet, initNetwork, saveFile } from '../util/util';

export interface TestNeuralNetworkArgs {
    testDataPath: string;

    neuralNetwork?: NeuralNetwork;
    networkFormat?: INetworkFormat;

    exportResultPath?: string;
}

type ConfusionFunction = (matrix: number[][], clazz: number) => number;

const truePositive: ConfusionFunction = (matrix, clazz) => matrix[clazz][clazz];
const trueNegative: ConfusionFunction = (matrix, clazz) => {
    let sum = 0;
    for (let i = 0; i < matrix.length; i++) {
        if (i === clazz) continue;
        for (let j = 0; j < matrix[i].length; j++) {
            if (j === clazz) continue;
            sum += matrix[i][j];
        }
    }
    return sum;
};

const falsePositive: ConfusionFunction = (matrix, clazz) => {
    let sum = 0;
    for (let i = 0; i < matrix.length; i++) {
        if (i === clazz) continue;
        sum += matrix[clazz][i];
    }
    return sum;
};

const falseNegative: ConfusionFunction = (matrix, clazz) => {
    let sum = 0;
    for (let i = 0; i < matrix.length; i++) {
        if (i === clazz) continue;
        sum += matrix[i][clazz];
    }
    return sum;
};

type ITestResult = Record<'class', string> & Record<'precision' | 'accuracy' | 'recall', number>;
type ITestResults = ITestResult[];

export const testNeuralNetwork = async (args: TestNeuralNetworkArgs): Promise<void> => {
    const network = initNetwork(args.neuralNetwork, args.networkFormat);

    const { inputs, outputs } = openDataSet(args.testDataPath, network.numInputs, network.numOutputs);

    const predictedOutputs = network
        .predict(inputs)
        .argMax(1)
        .as1D();

    const desiredOutputs = outputs
        .argMax(1)
        .as1D();

    const confusionMatrix = math
        .confusionMatrix(desiredOutputs, predictedOutputs, network.numOutputs)
        .arraySync() as number[][];

    const results: ITestResults = [];
    for (let i = 0; i < network.numOutputs; i++) {
        const tp = truePositive(confusionMatrix, i);
        const tn = trueNegative(confusionMatrix, i);
        const fp = falsePositive(confusionMatrix, i);
        const fn = falseNegative(confusionMatrix, i);

        results.push({
            class: i.toString(),
            accuracy: (tp + tn) / (tp + tn + fp + fn),
            precision: tp / (tp + fp),
            recall: tp + fn > 0
                ? tp / (tp + fn)
                : 0,
        });
    }

    if (!args.exportResultPath) {
        console.log(results);
    } else {
        saveFile(args.exportResultPath, JSON.stringify(results));
    }
};
