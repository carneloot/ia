import { NeuralNetwork } from '../neuralnetwork';
import { INetworkFormat } from '../interface/network-format.interface';
import { saveNetworkToFile } from '../util/util';

export interface NewNeuralNetworkArgs {
    networkFormat: INetworkFormat;

    exportNetwork?: string;
}

export const newNeuralNetwork = (args: NewNeuralNetworkArgs): void => {
    const { inputLayer, hiddenLayers, outputLayer } = args.networkFormat;

    const nn = new NeuralNetwork(inputLayer, hiddenLayers, outputLayer);

    if (args.exportNetwork) {
        saveNetworkToFile(args.exportNetwork, nn);
    } else {
        // TODO export to stdout
    }
};
