import { Tensor, Rank } from '@tensorflow/tfjs-node';

export interface IDataSet {
    inputs: Tensor<Rank>;
    outputs: Tensor<Rank>;
    length: number;
}
