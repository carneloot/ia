export interface ISerializedLayer {
    numInputs: number;
    numOutputs: number;
    weights: number[][];
    bias: number[][];
}

export interface ISerializedNeuralNetwork {
    numInputs: number;
    hiddenLayers: number[];
    numOutputs: number;
    layers: ISerializedLayer[];
}
