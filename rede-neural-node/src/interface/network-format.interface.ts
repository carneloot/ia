export interface INetworkFormat {
    inputLayer: number;
    hiddenLayers: number[];
    outputLayer: number;
}
