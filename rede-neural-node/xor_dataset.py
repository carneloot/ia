from random import randint

with open('./inputs/xor_train.csv', 'w') as arq:
    for i in range(300):
        a = randint(0, 1)
        b = randint(0, 1)

        output = '1,0'
        if a == b:
            output = '0,1'
        
        arq.write(f'{a},{b},{output}\n')
