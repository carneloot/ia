# Comandos

## Train

`train [train-data-path] [network-format]`

treina a IA utilizando os dados do arquivo em \[train-data-path\].

`[train-data-path]` caminho para o arquivo de entrada a ser usado para treinar.  Esse arquivo deve estar no formato CSV (separado por `,`) com as linhas sendo os valores, as colunas iniciais são as saidas e as colunas finais são as saidas.

`[network-format]` string com o formato da rede neural. Deve seguir o padrão:

`"num-inputs [num-escondida-1 [num-escondida-2 [num-escondida-3 ...] ] ] num-outputs"`

### Parâmetros
`--num-epochs=[number]` número de epocas a ser utilizado. Padrão = 1

`--export-network=[path]` caminho do arquivo json para exportar a configuração final da rede. Por padrão, os pesos serão escritos na tela

`--calculate-error` calcula o erro no fim de cada época. (Só tem isso porque o professor pediu)

`--validation-data=[path]` caminho para o arquivo de validação a ser usado. Caso seja especificado, será usado no lugar do train data para calcular o erro

`--neural-network=[path]` caminho para uma rede neural anteriormente exportada. Caso não seja especificado, a rede inicializará com valores aleatórios