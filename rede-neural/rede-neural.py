import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import json
from os.path import splitext

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_d(x):
    sig = sigmoid(x)
    return np.multiply(sig, (1 - sig))

# M => numero de neuronios (num_outputs)
# N => numero de entradas (num_inputs)

class RNA:
    def __init__(self, num_inputs, num_outputs):
        self.num_inputs = num_inputs
        self.num_outputs = num_outputs

        # First position is the bias
        self.weights = np.asmatrix(np.random.uniform(low=-1, high=1, size=(num_outputs, num_inputs + 1)))

        self.activation_function = sigmoid
        self.activation_derivative = sigmoid_d

    def train(self, input_values, actual_output_array, learning_rate):
        # Transforming into np.matrix
        real_input = np.asmatrix(np.append([1], input_values)).T
        actual_output = np.asmatrix(actual_output_array).T

        # Predicting
        predicted_output = self.predict(input_values)

        error_derivative = 2 * predicted_output - 2 * actual_output
        predicted_output_derivative = self.activation_derivative(predicted_output)

        grad_part_one = np.multiply(error_derivative, predicted_output_derivative)

        gradient = grad_part_one * real_input.T

        self.weights = self.weights - learning_rate * gradient

    def predict(self, input_values):
        # Since the first position is the bias, we need to add a one to the start of the input
        real_input = np.asmatrix(np.append([1], input_values)).T

        result = self.weights * real_input

        return self.activation_function(result)

    def predict10(self, input_values):
        predict = self.predict(input_values)
        predicted_index = np.argmax(predict)
        result = np.asmatrix(np.zeros((self.num_outputs, 1)))
        result[predicted_index, 0] = 1
        return result

    def get_linear_function(self, output_number):
        weights = self.weights[output_number,].A1
        return lambda x: ( -(weights[0]/weights[2])/(weights[0]/weights[1]) ) * x -weights[0]/weights[2]

TRAIN_DATA_PATH = 'dificil_treinamento.csv'
TEST_DATA_PATH = 'dificil_teste.csv'

NUM_EPOCHS = 300

NUM_INPUTS = 2
NUM_OUTPUTS = 5

COLORS = ['#F94144', '#43AA8B', '#577590', '#F8961E', '#90BE6D']

def train_rna(rna, inputs, outputs, num_epochs):
    print(f'Training RNA with {num_epochs} epochs')
    learning_rate = 0.2 / inputs.shape[0]

    for n in range(num_epochs):
        for x, d in zip(inputs, outputs):
            rna.train(x.tolist()[0], d.tolist()[0], learning_rate)

def prepare_data(path, num_input, num_output):
    print(f'Getting data from {path}')
    data = pd.read_csv(path, header=None)

    input_columns = range(num_input)
    input_data = np.asmatrix(data[input_columns])

    output_columns = range(num_input, num_input + num_output)
    output_data = np.asmatrix(data[output_columns])

    return (input_data, output_data)

def plot_data(rna, inputs, outputs, num_outputs):
    print('Plotting data')

    confusion_matrixes = []
    for i in range(num_outputs):
        confusion_matrixes.append({
            'tp': 0,
            'tn': 0,
            'fp': 0,
            'fn': 0,
        })

    samples = 200
    limit_offset = 3

    minxlim = abs(inputs[0,].min())
    maxxlim = abs(inputs[0,].max())
    minylim = abs(inputs[1,].min())
    maxylim = abs(inputs[1,].max())

    limit = max(minxlim, maxxlim, minylim, maxylim) + limit_offset

    plt.xlim(-limit, limit)
    plt.ylim(-limit, limit)

    points_per_class = [[] for i in range(num_outputs)]

    # Predicting
    for x, d in zip(inputs, outputs):
        predicted_output = rna.predict10(x.tolist()[0])

        # index = np.argmax(predicted_output) # Predicted
        index = np.argmax(d) # Actual

        points_per_class[index].append(x.A1.tolist())

        for i in range(num_outputs):
            predict = predicted_output[i, 0]
            actual = d[0, i]

            if predict == 1 and actual == 1:
                confusion_matrixes[i]['tp'] += 1
            elif predict == 0 and actual == 0:
                confusion_matrixes[i]['tn'] += 1
            elif predict == 1 and actual == 0:
                confusion_matrixes[i]['fp'] += 1
            elif predict == 0 and actual == 1:
                confusion_matrixes[i]['fn'] += 1

    # Plotting class functions
    for i, points_ in enumerate(points_per_class):
        x, y = np.array(points_).T
        plt.scatter(x, y,
            c=COLORS[i],
            label=f'Classe {i + 1}',
            s=10,
        )

    X = np.linspace(-limit, limit, samples)
    for i in range(num_outputs):
        f = rna.get_linear_function(i)
        Y = f(X)
        plt.plot(X, Y, color=COLORS[i], label=f'Função {i + 1} predita')

    plt.legend()

    return confusion_matrixes

if __name__ == '__main__':
    rna = RNA(NUM_INPUTS, NUM_OUTPUTS)

    filename = splitext(TEST_DATA_PATH)[0].replace('_', '-')
    test_input, test_output = prepare_data(TEST_DATA_PATH, NUM_INPUTS, NUM_OUTPUTS)

    train_input, train_output = prepare_data(TRAIN_DATA_PATH, NUM_INPUTS, NUM_OUTPUTS)

    # plot_data(rna, test_input, test_output, NUM_OUTPUTS)

    # plt.savefig(f'{filename}-{NUM_EPOCHS}-before.png')
    # plt.close()

    train_rna(rna, train_input, train_output, NUM_EPOCHS)

    # Testing / Plotting graph
    confusion = plot_data(rna, test_input, test_output, NUM_OUTPUTS)
    plt.savefig(f'./out/{filename}-{NUM_EPOCHS}-after.png')

    for clazz in confusion:
        clazz['precision'] = (clazz['tp'] / (clazz['tp'] + clazz['fp'])) or 0
        clazz['recall'] = (clazz['tp'] / (clazz['tp'] + clazz['fn'])) or 0

    export = {}
    export['confusion'] = confusion
    export['weights'] = rna.weights.tolist()

    with open(f'./out/{filename}-{NUM_EPOCHS}-info.json', 'w') as arq:
        json.dump(export, arq)