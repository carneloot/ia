import matplotlib.pyplot as plt
import numpy as np

limite, num_pontos = 50, 50

samples = 200

def reg_poli(pts, grau, filename):
    x = pts[:, 0]
    y = pts[:, 1]

    A = np.array([x ** (grau - i) for i in range(grau + 1)]).T

    m = np.matmul(np.matmul(np.linalg.inv(np.matmul(A.T, A)), A.T), y)

    def aplicar_polinomio(XX):
        valor = 0

        for i in range(len(m)):
            valor += m[i] * (XX ** (len(m) - i - 1))

        return valor

    X = np.linspace(-limite, limite, samples)
    Y = aplicar_polinomio(X)

    # Mostrando polinomio encontrado
    plt.xlim(-limite, limite)
    plt.ylim(-limite, limite)

    plt.plot(x, y, '+')
    plt.plot(X, Y)

    plt.grid()

    if filename is None:
        plt.show()
    else:
        plt.savefig(filename)
    
    plt.close()

if __name__ == "__main__":

    # Pegando pontos
    plt.xlim(-limite, limite)
    plt.ylim(-limite, limite)

    x_quadrado = lambda XX: - 0.05 * XX ** 2 + 40
    X = np.linspace(-limite, limite, samples)
    Y = x_quadrado(X)
    plt.plot(X, Y)

    plt.grid()

    pontos = plt.ginput(num_pontos)

    plt.close()

    pontos = np.array(pontos)

    # Regressao Linear
    reg_poli(pontos, 8, '4-8.png')

    reg_poli(pontos, 15, '4-15.png')

    reg_poli(pontos, 21, '4-21.png')
