import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class Perceptron:
    def __init__(self, num_inputs):
        self.num_inputs = num_inputs

        # First position is the bias
        self.weights = np.random.uniform(low=-1, high=1, size=(num_inputs + 1))

        self.activation_function = lambda x: np.tanh(x)
        self.activation_derivative = lambda x: 1.0 - (np.tanh(x) ** 2)

    def train(self, input, expected_result, learning_rate):
        value = self.predict(input)

        new_input = np.append([1], input)
        self.weights = self.weights - learning_rate * ( (2 * value) - (2 * expected_result) ) * self.activation_derivative(value) * new_input

    def predict(self, input):
        # Since the first position is the bias, we need to add a one to the start of the input
        new_input = np.append([1], input)
        dot = np.dot(self.weights, new_input)
        return self.activation_function(dot)

    def get_linear_function(self):
        return lambda x: ( -(self.weights[0]/self.weights[2])/(self.weights[0]/self.weights[1]) ) * x -self.weights[0]/self.weights[2]

NAME_DATA = 'data.csv'

def train_perceptron(perceptron, inputs, outputs, num_epochs):

    learning_rate = 0.2 / inputs.shape[0]

    for n in range(num_epochs):
        for i in range(inputs.shape[0]):
            perceptron.train(inputs[i], outputs[i], learning_rate)

if __name__ == '__main__':
    perceptron = Perceptron(2)

    # Getting the data
    data = pd.read_csv(NAME_DATA, header=None)
    outputs = np.array(data.pop(2))
    inputs = np.array(data)
    epochs = 35
    
    # Training
    train_perceptron(perceptron, inputs, outputs, epochs)

    # Plotting the points
    limit = 5
    samples = 200

    plt.xlim(-limit, limit)
    plt.ylim(-limit, limit)

    tp = 0
    tn = 0
    fp = 0
    fn = 0

    for i, (x, y) in enumerate(inputs):
        actual = outputs[i]

        # plotting
        color = 'r'
        if actual == 1:
            color = 'b'
        plt.plot(x, y, '.', color=color)

        # calculating informations
        predict = np.sign(perceptron.predict([x, y]))

        if predict == 1 and actual == 1:
            tp += 1
        elif predict == -1 and actual == -1:
            tn += 1
        elif predict == 1 and actual == -1:
            fp += 1
        elif predict == -1 and actual == 1:
            fn += 1

    precisionPositive = tp / (tp + fp)
    recallPositive = tp / (tp + fn)

    precisionNegative = tn / (tn + fn)
    recallNegative = tn / (tp + fn)

    print('true positive', tp)
    print('true negative', tn)
    print('false positive', fp)
    print('false negative', fn)

    print('precisionPositive', precisionPositive)
    print('recallPositive', recallPositive)
    print('precisionNegative', precisionNegative)
    print('recallNegative', recallNegative)

    # plotting predicted function
    f = perceptron.get_linear_function()
    X = np.linspace(-limit, limit, samples)
    Y = f(X)
    plt.plot(X, Y, color='black')

    plt.savefig('graph.png')

